OBJS := switcher.o
worms: $(OBJS)
	$(CC) -g -o switcher $(OBJS)

$(OBJS) : %.o : %.c
	$(CC) -g -c $(CFLAGS) $< -o $@
