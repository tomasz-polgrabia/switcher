#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <sys/time.h>

sig_atomic_t working = 1;

void sighandler(int nr) {
    switch (nr) {
        case SIGINT:
            fprintf (stderr, "Exit request...\n");
            working = 0;            
            break;
        default:
            fprintf (stderr, "Unknown signal to handle\n");
    }
}

int writeAll(char *path, char *str)
{
    int fd = open(path, O_WRONLY, 0777);
    if (fd < 0)
    {
        perror("failed to open file");
        return -1;
    }

    if (lseek(fd, 0, SEEK_SET) < 0)
    {
        perror("failed to seek ");
        return -1;
    }

    if (write(fd, str, strlen(str)) < 0)
    {
        perror("failed to write");
        return -1;
    }

    if (close(fd) < 0)
    {
        perror("failed to sync and close");
        return -1;
    }

    return 0;
}

int readAll(int fd, char *buffer, int max)
{
    int size;
    if ((size = lseek(fd, 0, SEEK_END)) < 0)
    {
        perror("failed to seek ");
        return -1;
    }

    if (lseek(fd, 0, SEEK_END) < 0)
    {
        perror("failed to seek ");
        return -1;
    }

    if (size > max)
        size = max;

    int count = -1;
    if ((count = read(fd, buffer, size)) < 0)
    {
        perror("failed to read");
        return -1;
    }

//    printf ("Wczytano %d\n", count);

    return 0;

}

int pollFd(int fd, int fd2, int fd3)
{

    if (lseek(fd, 0, SEEK_END) < 0)
    {
        perror("failed to seek ");
        return -1;
    }
    struct pollfd pollFds[6];
    pollFds[0].fd = fd;
    pollFds[0].events = POLLPRI;

    pollFds[1].fd = fd;
    pollFds[1].events = POLLERR;

    pollFds[2].fd = fd2;
    pollFds[2].events = POLLPRI;

    pollFds[3].fd = fd2;
    pollFds[3].events = POLLERR;

    pollFds[4].fd = fd3;
    pollFds[4].events = POLLPRI;

    pollFds[5].fd = fd3;
    pollFds[5].events = POLLERR;

    int ret = -1;
    int i = -1;

    if ((ret = poll(pollFds, 6, 3600000)) < 0)
    {
        if (errno == EINTR)
            return -1;
        perror("failed to poll");
        return -1;
    }

    for (i = 0; i < 6; i++)
    {
        if (pollFds[i].events == pollFds[i].revents)
            return i / 2;
    }

//    printf ("Zwrocona wartosc ret to: %d\n", ret);

//    printf ("returned event is: %d\n", pollFds[0].revents);

    return 0;

}

int main() {
    signal(SIGINT, sighandler);

    printf ("Hello build: %d\n", 668);

    int fdExport = open("/sys/class/gpio/export",  O_WRONLY, 0777);
    if (fdExport < 0)
    {
        perror("I cannot open /sys/class/gpio/export to write");
        return 1;
    }

    lseek(fdExport, 0, SEEK_SET);
    if (write(fdExport, "27\n", 3) < 0)
    {
        perror("error export for device 27");
        return 1;
    }
    fsync(fdExport);

    lseek(fdExport, 0, SEEK_SET); // zielona
    if (write(fdExport, "23\n", 3) < 0)
    {
        perror("error export for device 23");
        return 1;
    }
    fsync(fdExport);

    lseek(fdExport, 0, SEEK_SET); //czerwona
    if (write(fdExport, "24\n", 3) < 0)
    {
        perror("error export for device 24");
        return 1;
    }
    fsync(fdExport);

    lseek(fdExport, 0, SEEK_SET); // SW-22
    if (write(fdExport, "22\n", 3) < 0)
    {
        perror("error export for device 22");
        return 1;
    }
    fsync(fdExport);

    lseek(fdExport, 0, SEEK_SET); // SW-10
    if (write(fdExport, "10\n", 3) < 0)
    {
        perror("error export for device 24");
        return 1;
    }
    fsync(fdExport);




    close(fdExport);

    if (writeAll("/sys/class/gpio/gpio27/direction", "in\n") < 0)
    {
        perror("failed to change direction to out for 27");
        return 1;
    }

    if (writeAll("/sys/class/gpio/gpio27/edge", "rising\n") < 0)
    {
        perror("failed to change direciton for exception");
        return -1;
    }

    if (writeAll("/sys/class/gpio/gpio22/direction", "in\n") < 0)
    {
        perror("failed to change direction to out for 22");
        return 1;
    }

    if (writeAll("/sys/class/gpio/gpio22/edge", "rising\n") < 0)
    {
        perror("failed to change direciton for exception");
        return -1;
    }

    if (writeAll("/sys/class/gpio/gpio10/direction", "in\n") < 0)
    {
        perror("failed to change direction to out for 27");
        return 1;
    }

    if (writeAll("/sys/class/gpio/gpio10/edge", "rising\n") < 0)
    {
        perror("failed to change direciton for exception");
        return -1;
    }

    if (writeAll("/sys/class/gpio/gpio23/direction", "out\n") < 0)
    {
        perror("failed to change direction to out for 23");
        return 1;
    }

    if (writeAll("/sys/class/gpio/gpio24/direction", "out\n") < 0)
    {
        perror("failed to change direction to out for 24");
        return 1;
    }

    struct timeval val;

    int fd3 = open("/sys/class/gpio/gpio27/value", O_RDONLY, 0777);
    if (fd3 < 0)
    {
        perror("failed to open file");
        return -1;
    }

    int fd4 = open("/sys/class/gpio/gpio22/value", O_RDONLY, 0777);
    if (fd4 < 0)
    {
        perror("failed to open file");
        return -1;
    }

    int fd5 = open("/sys/class/gpio/gpio10/value", O_RDONLY, 0777);
    if (fd5 < 0)
    {
        perror("failed to open file");
        return -1;
    }

                 if (writeAll("/sys/class/gpio/gpio24/value", "1\n") < 0)
                {
                    perror("Error turning green");
                    return 1;
                }



    char buffer[0x1000];

    struct timeval last;
    if (gettimeofday(&last, NULL) < 0)
    {
        perror("Blad przy pobieranu czasu");
        return 1;
    }

    int state = 0;

    while (working) {
        int ret = -1;
        fprintf (stderr, "Polling for event\n");
        if ((ret = pollFd(fd3, fd4, fd5)) < 0)
        {
            if (errno == EINTR)
                continue;
            perror("Failed to poll 27");
            return -1;
        }

        printf ("Result: %d\n", ret);

        double diff;
        struct timeval curr;

        if (gettimeofday(&curr, NULL) < 0)
        {
            perror("Failed to read time");
            return 1;
        }

        diff = (curr.tv_sec - last.tv_sec)*1000000;
        diff += curr.tv_usec - last.tv_usec;

        if (ret == 0)
            fprintf (stderr, "Last lap time - %lfms\n", diff / 1000.0);

        if (readAll(fd3, buffer, 0x1000) < 0)
        {
            perror("readall");
            return 1;
        }

        if (readAll(fd4, buffer, 0x1000) < 0)
        {
            perror("readall");
            return 1;
        }

        if (readAll(fd5, buffer, 0x1000) < 0)
        {
            perror("readall");
            return 1;
        }



        if (ret > 0)
        {
            last = curr;
            if (ret == 1)
            {
                // włączenie
                printf ("Turn on");
                state = 1;
                if (writeAll("/sys/class/gpio/gpio23/value", "1\n") < 0) 
                {
                    perror("Error turning red");
                    return 1;
                }

                if (writeAll("/sys/class/gpio/gpio24/value", "0\n") < 0)
                {
                    perror("Error turning red");
                    return 1;
                }

            }
            else
            {
                // wyłączenie
                printf ("Reset");
                state = 0;
                 if (writeAll("/sys/class/gpio/gpio24/value", "1\n") < 0)
                {
                    perror("Error turning green");
                    return 1;
                }

                 if (writeAll("/sys/class/gpio/gpio23/value", "0\n") < 0)
                {
                    perror("Error turning green");
                    return 1;
                }

               
            }
        } 

//        printf ("Value: %s\n", buffer);

    }

    close(fd3);
    close(fd4);
    close(fd5);

                 if (writeAll("/sys/class/gpio/gpio24/value", "0\n") < 0)
                {
                    perror("Error turning green");
                    return 1;
                }

                 if (writeAll("/sys/class/gpio/gpio23/value", "0\n") < 0)
                {
                    perror("Error turning green");
                    return 1;
                }



    int fdUnExport = open("/sys/class/gpio/unexport",  O_WRONLY, 0777);
    if (fdUnExport < 0)
    {
        perror("I cannot open /sys/class/gpio/unexport to write");
        return 1;
    }

    lseek(fdUnExport, 0, SEEK_SET);
    if (write(fdUnExport, "27\n", 3) < 0)
    {
        perror("error unexport for device 27");
        return 1;
    }
    fsync(fdUnExport);

    lseek(fdUnExport, 0, SEEK_SET); // zielona
    if (write(fdUnExport, "23\n", 3) < 0)
    {
        perror("error unexport for device 23");
        return 1;
    }
    fsync(fdUnExport);

    lseek(fdUnExport, 0, SEEK_SET);
    if (write(fdUnExport, "24\n", 3) < 0)
    {
        perror("error unexport for device 24");
        return 1;
    }
    fsync(fdUnExport);

    lseek(fdUnExport, 0, SEEK_SET); // zielona
    if (write(fdUnExport, "22\n", 3) < 0)
    {
        perror("error unexport for device 23");
        return 1;
    }
    fsync(fdUnExport);

    lseek(fdUnExport, 0, SEEK_SET);
    if (write(fdUnExport, "10\n", 3) < 0)
    {
        perror("error unexport for device 24");
        return 1;
    }
    fsync(fdUnExport);


    close(fdUnExport);


}
